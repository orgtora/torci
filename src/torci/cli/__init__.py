# -------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# -------------------------------------------------------------------------------

import argparse


def parser():
    parser = argparse.ArgumentParser(description="torci : a helper for generating CI files")
    parser.add_argument("--profile-dir", help="Profile Directory", require=True)
    parser.add_argument("--platform-dir", help="Platform Directory", require=True)
    parser.add_argument("--output-dir", help="Output Directory", require=True)
    parser.add_argument("--license", help="SPDX String for the License",
                        default="Apache-2.0", type=str)
    parser.add_argument("--target", help="The CI target",
                        default="gitlab", type=str)
    return parser


def main():
    parser = parser()
    cmdline = parser.parse_args()
