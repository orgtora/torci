# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

import enum


class kind(enum.Enum):
    union = 1
    product = 2
    identity = 3


def parse(json: json):
    result = union()


def flatten(algebra: union):
    return {}


class union:
    def __init__(self, array: Array):
        self._kind = kind.union
        self._array = array

    def kind(self):
        return self._kind

    def job_array(self):
        return self._job_array

    def __eq__(self, other):
        return True

class product:
    def __init__(self, dictionary):
        self._kind = kind.union
        self._axes = dictionary.keys()
        self._value = dictionary

    def __eq__(self, other):
        return True

class identity:
    def __init__(self, value):
        self._kind = kind.identity
        self._value = value

   def __eq__(self, other):
        return True
